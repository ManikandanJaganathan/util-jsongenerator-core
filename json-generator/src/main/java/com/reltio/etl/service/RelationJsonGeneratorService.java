package com.reltio.etl.service;

import static com.reltio.etl.constants.JsonGeneratorProperties.GROUP_TO_SENT;
import static com.reltio.etl.constants.JsonGeneratorProperties.MAX_QUEUE_SIZE_MULTIPLICATOR;
import static com.reltio.etl.service.util.JSONGeneratorUtil.printDataloadPerformance;
import static com.reltio.etl.service.util.JSONGeneratorUtil.waitForTasksReady;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.reltio.cst.util.Util;
import com.reltio.etl.constants.JsonGeneratorProperties;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.ReltioObject;
import com.reltio.etl.service.domain.RelationConfigurationProperties;
import com.reltio.etl.service.util.JSONGeneratorUtil;
import com.reltio.etl.util.NormalizedJsonConversationHelper;
import com.reltio.file.ReltioCSVFileReader;
import com.reltio.file.ReltioCSVFileWriter;
import com.reltio.file.ReltioFileReader;
import com.reltio.file.ReltioFileWriter;
import com.reltio.file.ReltioFlatFileReader;
import com.reltio.file.ReltioFlatFileWriter;

/**
 * This is the main class for generating json for Relations. Handles both
 * Normalized and denormalized file columns
 */
public class RelationJsonGeneratorService {

    private static final Logger logger = LogManager.getLogger(RelationJsonGeneratorService.class.getName());

    private static Gson gson = new Gson();
    // Keep column name and index in the input file
    private final Map<String, Integer> columnIndexMap = new HashMap<>();
    // Keep non-valid column details list
    private final Set<String> nonColumnValues = new HashSet<>();
    // Keeps Normalized Value columns
    private final Set<String> normalizedColumns = new HashSet<>();
    private final List<ReltioObject> reltioOjectsOfFile = new ArrayList<>();
    // keeps the Source System of the File
    private String fileSourceSystem = null;

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static void main(String[] args) throws Exception {

        logger.info("Process Started");
        long programStartTime = System.currentTimeMillis();
        int count = 0;
        final RelationJsonGeneratorService jsonGeneratorService = new RelationJsonGeneratorService();
        final int[] rejectedRecordCount = new int[1];
        rejectedRecordCount[0] = 0;

        Properties properties = new Properties();
        FileReader fileReader = null;
        try {
            String propertyFilePath = args[0];
            fileReader = new FileReader(
                    StringEscapeUtils.escapeJava(propertyFilePath));
            properties.load(fileReader);
        } catch (Exception e) {
            logger.error("Failed Read the Properties File :: ");
            logger.error(e.getMessage());
            logger.debug(e);
            //e.printStackTrace();
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    logger.error(e.getMessage());
                    logger.debug(e);
                }
            }

        }
        // READ the Config Properties values
        final RelationConfigurationProperties configProperties = new RelationConfigurationProperties(
                properties);

        if (args != null && args.length > 1) {
            configProperties.setInputDataFilePath(args[1]);
            configProperties.setMappingFilePath(args[2]);
            configProperties.setOutputFilePath(args[3]);
        }
        
		Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();
		mutualExclusiveProps.put(Arrays.asList("START_OBJECT_CROSSWALK_VALUE_COLUMN"), Arrays.asList("START_OBJECT_ENTITY_URI_COLUMN"));
		mutualExclusiveProps.put(Arrays.asList("END_OBJECT_CROSSWALK_VALUE_COLUMN"), Arrays.asList("END_OBJECT_ENTITY_URI_COLUMN"));


        List<String> requiredProps = Arrays.asList(
                "INPUT_DATA_FILE",
                "RELATION_TYPE",
                "INPUT_FILE_FORMAT",
                "OUTPUT_FILE",
                "SOURCE_SYSTEM");

        List<String> missingProps = Util.listMissingProperties(properties, requiredProps, mutualExclusiveProps);

        if (missingProps.size() > 0) {
            logger.error("Process Aborted due to insufficient input properties... Below are the list of missing properties");
            logger.error(String.join("\n", missingProps));
            System.exit(-1);
        }

        jsonGeneratorService.fileSourceSystem = configProperties
                .getSourceSystem();

        // Read the Mapping File configuration
        properties.clear();
        if (configProperties.getMappingFilePath() != null
                && !configProperties.getMappingFilePath().isEmpty()) {
            try {
                fileReader = new FileReader(
                        StringEscapeUtils.escapeJava(configProperties
                                .getMappingFilePath()));
                properties.load(fileReader);
            } catch (Exception e) {
                logger.error("Failed to Read the Properties File :: "
                        + configProperties.getMappingFilePath());
                logger.error(e.getMessage());
                logger.debug(e);
                System.exit(-1);
            } finally {
                if (fileReader != null) {
                    try {
                        fileReader.close();
                    } catch (IOException e) {
                        logger.error(e.getMessage());
                        logger.debug(e);
                    }
                }
            }
        }
        // Create Reader for input data File
        ReltioFileReader inputDataFileReader = null;
        final ReltioFileWriter failedRecordsWriter;
        String failedRecordsFileName;

        if (configProperties.getInputFileFormat().equals("CSV")) {
            inputDataFileReader = new ReltioCSVFileReader(
                    configProperties.getInputDataFilePath(), "UTF-8");
            failedRecordsFileName = configProperties.getOutputFilePath()
                    + "_RejectedRecords.csv";

            failedRecordsWriter = new ReltioCSVFileWriter(failedRecordsFileName);
        } else {

            inputDataFileReader = new ReltioFlatFileReader(
                    configProperties.getInputDataFilePath(),
                    configProperties.getInputFileDelimiter(), "UTF-8");
            failedRecordsFileName = configProperties.getOutputFilePath()
                    + "_RejectedRecords.txt";
            failedRecordsWriter = new ReltioFlatFileWriter(
                    failedRecordsFileName, "UTF-8",
                    configProperties.getInputFileDelimiter());


        }

        // Create Writter for outputFile
        final ReltioFileWriter reltioFileWriter = new ReltioFlatFileWriter(
                configProperties.getOutputFilePath(), "UTF-8");

        String[] lineValues = null;

        // Read Header
        lineValues = inputDataFileReader.readLine();

        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                .addValueInArrayBegining(
                        JsonGeneratorProperties.FAILED_RECORD_REASON,
                        lineValues));

        final int sizeOfHeaderColumn = lineValues.length;

        List<String> allColumnNames = Arrays.asList(lineValues);

        // Create a Map for Column Name to Index
        for (int i = 0; i < allColumnNames.size(); i++) {
            jsonGeneratorService.columnIndexMap.put(allColumnNames.get(i), i);
        }

        final Map<String, String> attributeMappingDetails = new HashMap<>(
                (Map) properties);

        // Populate all the attributes from mapping file to Reltio Dummy Object
        final Map<String, Object> attributesTemplateList;
        if (configProperties.getMappingFilePath() != null) {
            // Populate all the attributes from mapping file to Reltio Dummy
            // Object
            attributesTemplateList = JsonGeneratorService
                    .createReltioTemplateObjectAttributesList(
                            attributeMappingDetails,
                            jsonGeneratorService.columnIndexMap,
                            jsonGeneratorService.nonColumnValues,
                            null,
                            jsonGeneratorService.normalizedColumns);
            if ((configProperties.getColumnDelimiter() == null || configProperties
                    .getColumnDelimiter().isEmpty())
                    && !jsonGeneratorService.normalizedColumns.isEmpty()) {
                logger.error("Below File columns provided in the mapping File mentioned as Normalized Columns. But the NORMALIZED_FILE_COLUMN_DELIMITER property is empty..");

                for (String column : jsonGeneratorService.normalizedColumns) {
                    logger.error(column);
                }

                logger.error("Please update the Job Configuration file with the NORMALIZED_FILE_COLUMN_DELIMITER value and Try again...");
                System.exit(-1);

            }

            // Check the crosswalk Column
            if (configProperties.getCrosswalkValueColumn() != null
                    && !configProperties.getCrosswalkValueColumn().isEmpty()
                    && jsonGeneratorService.columnIndexMap.get(configProperties
                    .getCrosswalkValueColumn()) == null) {
                jsonGeneratorService.nonColumnValues.add(configProperties
                        .getCrosswalkValueColumn());
            }

            boolean isNonColumnValuePresent = false;
            if (configProperties.getStartObjCrosswalkValueColumn() != null
                    && jsonGeneratorService.columnIndexMap.get(configProperties
                    .getStartObjCrosswalkValueColumn()) == null) {
                jsonGeneratorService.nonColumnValues.add(configProperties
                        .getStartObjCrosswalkValueColumn());
            }

            if (configProperties.getEndObjCrosswalkValueColumn() != null
                    && jsonGeneratorService.columnIndexMap.get(configProperties
                    .getEndObjCrosswalkValueColumn()) == null) {
                jsonGeneratorService.nonColumnValues.add(configProperties
                        .getEndObjCrosswalkValueColumn());
            }

            if (!jsonGeneratorService.nonColumnValues.isEmpty()) {

                for (String col : jsonGeneratorService.nonColumnValues) {
                    if (!col.startsWith("{") || !col.endsWith("}")) {
                        if (!isNonColumnValuePresent) {
                            logger.error("Non-Column Values in the Mapping File Below::::");
                        }
                        logger.error(col);
                        isNonColumnValuePresent = true;
                    }
                }
            }

            if (isNonColumnValuePresent) {
                logger.error("Please validate the Mapping File Column Names listed above and re-try.....");
                System.exit(-1);
            }

        } else {
            attributesTemplateList = null;
        }

        // Start and End Object crosswalk//Entity URI columns
        final Integer startObjectCrosswalkIndex, startObjectEntityUriIndex, endObjecCrosswalkIndex, endObjectEntityUriIndex;

        if (configProperties.getStartObjCrosswalkValueColumn() != null) {
            startObjectCrosswalkIndex = jsonGeneratorService.columnIndexMap
                    .get(configProperties.getStartObjCrosswalkValueColumn());
            startObjectEntityUriIndex = null;
        } else {
            startObjectEntityUriIndex = jsonGeneratorService.columnIndexMap
                    .get(configProperties.getStartObjEntityUriColumn());
            startObjectCrosswalkIndex = null;
        }

        if (configProperties.getEndObjCrosswalkValueColumn() != null) {
            endObjecCrosswalkIndex = jsonGeneratorService.columnIndexMap
                    .get(configProperties.getEndObjCrosswalkValueColumn());
            endObjectEntityUriIndex = null;
        } else {
            endObjectEntityUriIndex = jsonGeneratorService.columnIndexMap
                    .get(configProperties.getEndObjEntityUriColumn());
            endObjecCrosswalkIndex = null;
        }

        // Thread Operations
        ThreadPoolExecutor executorService = (ThreadPoolExecutor) Executors
                .newFixedThreadPool(configProperties.getThreadCount());
        ArrayList<Future<Long>> futures = new ArrayList<Future<Long>>();
        boolean eof = false;

        // Create Reltio Dummy Object
        //todo createdDate and updatedDate are not implemented in relation json gene
        final Crosswalk crosswalk = new Crosswalk();
        crosswalk.createDate = configProperties.getCrosswalkCreateDate();
        crosswalk.updateDate = configProperties.getCrosswalkUpdateDate();
        crosswalk.deleteDate = configProperties.getCrosswalkDeleteDate();
        crosswalk.type = configProperties.getSourceSystem();
        List<String[]> inputLineValues = new ArrayList<>();

        final Integer crosswalkValueColummnIndex;
        if (configProperties.getCrosswalkValueColumn() != null
                && !configProperties.getCrosswalkValueColumn().isEmpty()) {
            crosswalkValueColummnIndex = jsonGeneratorService.columnIndexMap
                    .get(configProperties.getCrosswalkValueColumn());
        } else {
            crosswalkValueColummnIndex = null;
        }

        final Integer startDateIndex = jsonGeneratorService.columnIndexMap
                .get(configProperties.getStartDateColumn());
        final Integer endDateIndex = jsonGeneratorService.columnIndexMap
                .get(configProperties.getEndDateColumn());

        long totalFuturesExecutionTime = 0L;
        while (!eof) {
            for (int threadNum = 0; threadNum < configProperties
                    .getThreadCount() * MAX_QUEUE_SIZE_MULTIPLICATOR; threadNum++) {
                inputLineValues.clear();

                for (int k = 0; k < GROUP_TO_SENT; k++) {

                    // Read line
                    lineValues = inputDataFileReader.readLine();
                    if (lineValues == null) {
                        eof = true;
                        break;
                    }
                    count++;
                    inputLineValues.add(lineValues);

                }

                if (!inputLineValues.isEmpty()) {
                    final List<String[]> threadInputLines = new ArrayList<>(
                            inputLineValues);

                    futures.add(executorService.submit(new Callable<Long>() {

                        @Override
                        public Long call() {
                            long requestExecutionTime = 0l;
                            long startTime = System.currentTimeMillis();
                            try {
                                ReltioObject reltioObject;
                                Crosswalk reltioObjectCross = null;
                                List<ReltioObject> reltioObjects = null;

                                List<String[]> outputLineValues = new ArrayList<>();

                                List<ReltioObject> reltioObjectsOfallRecords = new ArrayList<>();

                                for (String[] lineValues : threadInputLines) {
                                    String startObj = null;
                                    String endObj = null;
                                    String[] output;
                                    if (lineValues.length != sizeOfHeaderColumn) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                                                .addValueInArrayBegining(
                                                        " Number of column values not matching with header column count:: Header count: "
                                                                + sizeOfHeaderColumn
                                                                + " .. Current Row column values count: "
                                                                + lineValues.length,
                                                        lineValues));
                                        logger.error("Rejected: Number of column values not matching with header column count:|"
                                                + Arrays.toString(lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    reltioObject = new ReltioObject();
                                    reltioObjectCross = new Crosswalk();
                                    reltioObjects = new ArrayList<>();

                                    reltioObject.type = configProperties
                                            .getRelationType();
                                    if (crosswalk.createDate != null
                                            && jsonGeneratorService.columnIndexMap
                                            .get(crosswalk.createDate) != null) {
                                        reltioObjectCross.createDate = lineValues[jsonGeneratorService.columnIndexMap
                                                .get(crosswalk.createDate)];
                                    }

                                    if (crosswalk.updateDate != null
                                            && jsonGeneratorService.columnIndexMap
                                            .get(crosswalk.updateDate) != null) {
                                        reltioObjectCross.updateDate = lineValues[jsonGeneratorService.columnIndexMap
                                                .get(crosswalk.updateDate)];
                                    }

                                    if (crosswalk.deleteDate != null
                                            && jsonGeneratorService.columnIndexMap
                                            .get(crosswalk.deleteDate) != null) {
                                        reltioObjectCross.deleteDate = lineValues[jsonGeneratorService.columnIndexMap
                                                .get(crosswalk.deleteDate)];
                                    }

                                    if (startObjectCrosswalkIndex != null) {
                                        if (NormalizedJsonConversationHelper
                                                .checkNotNull(lineValues[startObjectCrosswalkIndex])) {
                                            startObj = lineValues[startObjectCrosswalkIndex]
                                                    .trim();
                                            String sourceSystem;
                                            String sourceTable = configProperties.getStartObjSourceTable();
                                            if (configProperties
                                                    .getStartObjSourceSystem()
                                                    .contains(
                                                            "configuration/sources/")) {
                                                sourceSystem = configProperties
                                                        .getStartObjSourceSystem();
                                            } else {
                                                sourceSystem = lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
                                                        .getStartObjSourceSystem())];
                                                if (!sourceSystem
                                                        .contains("configuration/sources/")) {
                                                    sourceSystem = "configuration/sources/"
                                                            + sourceSystem
                                                            .trim();
                                                }
                                            }
                                            if (NormalizedJsonConversationHelper.checkNotNull(sourceTable)) {
                                                reltioObject
                                                        .addRelationStartObjectCrosswalk(
                                                                sourceSystem,
                                                                startObj, sourceTable);
                                            } else {
                                                reltioObject
                                                        .addRelationStartObjectCrosswalk(
                                                                sourceSystem,
                                                                startObj);
                                            }
                                        }
                                    } else {
                                        if (NormalizedJsonConversationHelper
                                                .checkNotNull(lineValues[startObjectEntityUriIndex])) {
                                            try {
                                                startObj = lineValues[startObjectEntityUriIndex]
                                                        .trim();
                                            } catch (Exception e) {
                                                logger.error(e.getMessage());
                                                logger.debug(e);
                                            }
                                            reltioObject
                                                    .addRelationStartObjectURI(startObj);

                                        }
                                    }

                                    if (endObjecCrosswalkIndex != null) {
                                        if (NormalizedJsonConversationHelper
                                                .checkNotNull(lineValues[endObjecCrosswalkIndex])) {
                                            try {
                                                endObj = lineValues[endObjecCrosswalkIndex]
                                                        .trim();
                                            } catch (Exception e) {
                                                logger.error(e.getMessage());
                                                logger.debug(e);
                                            }

                                            String sourceSystem;
                                            String sourceTable = configProperties.getEndObjSourceTable();
                                            if (configProperties
                                                    .getEndObjSourceSystem()
                                                    .contains(
                                                            "configuration/sources/")) {
                                                sourceSystem = configProperties
                                                        .getEndObjSourceSystem();
                                            } else {
                                                sourceSystem = lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
                                                        .getEndObjSourceSystem())];
                                                if (!sourceSystem
                                                        .contains("configuration/sources/")) {
                                                    sourceSystem = "configuration/sources/"
                                                            + sourceSystem
                                                            .trim();
                                                }
                                            }

                                            if (NormalizedJsonConversationHelper.checkNotNull(sourceTable)) {
                                                reltioObject
                                                        .addRelationEndObjectCrosswalk(
                                                                sourceSystem,
                                                                endObj, sourceTable);
                                            } else {
                                                reltioObject
                                                        .addRelationEndObjectCrosswalk(
                                                                sourceSystem,
                                                                endObj);
                                            }
                                        }
                                    } else {
                                        if (NormalizedJsonConversationHelper
                                                .checkNotNull(lineValues[endObjectEntityUriIndex])) {
                                            endObj = lineValues[endObjectEntityUriIndex]
                                                    .trim();
                                            reltioObject
                                                    .addRelationEndObjectURI(endObj);

                                        }
                                    }

                                    if (startObj == null
                                            || startObj.trim().isEmpty()) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                                                .addValueInArrayBegining(
                                                        "Relation Start Object Crosswalk Column Empty",
                                                        lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    if (endObj == null
                                            || endObj.trim().isEmpty()) {
                                        failedRecordsWriter.writeToFile(JSONGeneratorUtil
                                                .addValueInArrayBegining(
                                                        " Relation End Object Crosswalk Column Empty",
                                                        lineValues));
                                        rejectedRecordCount[0]++;
                                        continue;
                                    }

                                    /*
                                     * If isIgnoreRelationCrosswalk is true then donot add crosswalk section in JSON
                                     */
                                    if (!configProperties.isIgnoreRelationCrosswalk()) {
                                        reltioObjectCross.type = configProperties
                                                .getSourceSystem();
                                        if (crosswalkValueColummnIndex != null
                                                && lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
                                                .getCrosswalkValueColumn())] != null) {
                                            reltioObjectCross.value = lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
                                                    .getCrosswalkValueColumn())]
                                                    .trim();

                                        } else {
                                            reltioObjectCross.value = configProperties
                                                    .getRelationType()
                                                    .substring(
                                                            configProperties
                                                                    .getRelationType()
                                                                    .lastIndexOf(
                                                                            "/") + 1,
                                                            configProperties
                                                                    .getRelationType()
                                                                    .length())
                                                    + "_"
                                                    + reltioObjectCross.type
                                                    .substring(
                                                            reltioObjectCross.type
                                                                    .lastIndexOf("/") + 1,
                                                            reltioObjectCross.type
                                                                    .length())
                                                    + "_" + startObj + "_" + endObj;
                                        }

                                        String sourceSystem;
                                        String sourceTable = configProperties.getSourceTable();
                                        if (configProperties
                                                .getSourceSystem()
                                                .contains(
                                                        "configuration/sources/")) {
                                            sourceSystem = configProperties
                                                    .getSourceSystem();
                                        } else {
                                            sourceSystem = lineValues[jsonGeneratorService.columnIndexMap.get(configProperties
                                                    .getSourceSystem())];
                                            if (!sourceSystem
                                                    .contains("configuration/sources/")) {
                                                sourceSystem = "configuration/sources/"
                                                        + sourceSystem
                                                        .trim();
                                            }
                                        }

                                        if (NormalizedJsonConversationHelper.checkNotNull(sourceTable)) {
                                            reltioObject.addCrosswalks(sourceSystem, reltioObjectCross.value.trim(),
                                                    reltioObjectCross.createDate,
                                                    reltioObjectCross.updateDate,
                                                    reltioObjectCross.deleteDate,
                                                    sourceTable);
                                        } else {

                                            reltioObject.addCrosswalks(
                                                    sourceSystem,
                                                    reltioObjectCross.value.trim(),
                                                    reltioObjectCross.createDate,
                                                    reltioObjectCross.updateDate,
                                                    reltioObjectCross.deleteDate);

                                        }

                                    }


                                    if (attributesTemplateList != null
                                            && !attributesTemplateList
                                            .isEmpty()) {
                                        try {
                                            reltioObject
                                                    .createAttributes(
                                                            attributesTemplateList,
                                                            lineValues,
                                                            jsonGeneratorService.columnIndexMap,
                                                            jsonGeneratorService.nonColumnValues,
                                                            crosswalk,
                                                            configProperties
                                                                    .getColumnDelimiter(), configProperties.isIgnoreNullAttribute(), null);
                                            
                                        } catch (Exception e) {
                                            logger.error(e.getMessage());
                                            logger.debug(e);
                                            failedRecordsWriter
                                                    .writeToFile(JSONGeneratorUtil
                                                            .addValueInArrayBegining(
                                                                    " Generic Error: "
                                                                            + e.getMessage(),
                                                                    lineValues));
                                            logger.error("Rejected: Failed to do Attribute mapping|"
                                                    + Arrays.toString(lineValues));
                                            rejectedRecordCount[0]++;

                                        }
                                    }

                                    if (startDateIndex != null
                                            && NormalizedJsonConversationHelper
                                            .checkNotNull(lineValues[startDateIndex])) {
                                        reltioObject
                                                .setStartDate(lineValues[startDateIndex]);
                                    }

                                    if (endDateIndex != null
                                            && NormalizedJsonConversationHelper
                                            .checkNotNull(lineValues[endDateIndex])) {
                                        reltioObject
                                                .setEndDate(lineValues[endDateIndex]);
                                    }

                                    if (configProperties
                                            .getIsSingleJsonOutput()) {
                                        reltioObjectsOfallRecords
                                                .add(reltioObject);
                                    } else {
                                        reltioObjects.add(reltioObject);

                                        String json = gson.toJson(reltioObjects);
                                        json = json.replaceAll(
                                                "\"" + JsonGeneratorProperties.NULL_PLACEHOLDER_VALUE + "\"",
                                                "null");

                                        if (configProperties
                                                .getIsJsonWithOutCrosswalk()) {
                                            output = new String[1];
                                            output[0] = json;

                                        } else {
                                            output = new String[2];
                                            output[0] = reltioObjectCross.value;
                                            output[1] = json;
                                        }
                                        outputLineValues.add(output);
                                    }
                                }
                                if (!configProperties.getIsSingleJsonOutput()) {
                                    reltioFileWriter
                                            .writeToFile(outputLineValues);
                                } else {
                                    jsonGeneratorService
                                            .addDataToReltioObjectList(reltioObjectsOfallRecords);
                                }
                                requestExecutionTime = System
                                        .currentTimeMillis() - startTime;
                            } catch (Exception e) {
                                logger.error(e.getMessage());
                                logger.debug(e);
                                logger.error("Unexpected Error happened .... "
                                        + e.getMessage());
                            }
                            return requestExecutionTime;
                        }
                    }));

                }

                if (eof) {
                    break;
                }

            }

            logger.info("Number of records read from file =" + count);
            totalFuturesExecutionTime += waitForTasksReady(futures, configProperties.getThreadCount()
                    * (MAX_QUEUE_SIZE_MULTIPLICATOR / 2));
            printDataloadPerformance(executorService.getCompletedTaskCount() * GROUP_TO_SENT, totalFuturesExecutionTime, programStartTime, configProperties.getThreadCount());

        }

        totalFuturesExecutionTime += waitForTasksReady(futures, 0);
        printDataloadPerformance(executorService.getCompletedTaskCount() * GROUP_TO_SENT, totalFuturesExecutionTime, programStartTime, configProperties.getThreadCount());

        if (configProperties.getIsSingleJsonOutput()) {
            String json = gson
                    .toJson(jsonGeneratorService.reltioOjectsOfFile);
            reltioFileWriter.writeToFile(json.replaceAll("\"" + JsonGeneratorProperties.NULL_PLACEHOLDER_VALUE + "\"", "null"));
        }

        reltioFileWriter.close();
        inputDataFileReader.close();
        failedRecordsWriter.close();
        logger.info("\n \n *** Final Metrics of the JSON Generation ***");
        logger.info("Total Number of Records in the File = " + count);
        logger.info("Total Number of Rejected Records = " + rejectedRecordCount[0]);
        logger.info("Total Number of records processed successfully = " + (count - rejectedRecordCount[0]));
        logger.info("Total Time Taken in (ms) = " + (System.currentTimeMillis() - programStartTime));
        executorService.shutdown();
        if (rejectedRecordCount[0] == 0) {
            new File(failedRecordsFileName).delete();
        }
        logger.info("Process Completed");
    }

    private synchronized void addDataToReltioObjectList(
            List<ReltioObject> objects) {
        reltioOjectsOfFile.addAll(objects);
    }
}
