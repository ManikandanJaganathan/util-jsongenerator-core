package com.reltio.etl.domain;

/**
 * 
 * This is the Template class Crosswalks section in the Entity JSON
 */
public class Crosswalk {

	public String type;
	public String value;
	public String createDate;
	public String updateDate;
	public String deleteDate;
	public Boolean dataProvider;
	public String sourceTable;
	public Boolean contributorProvider;
}
