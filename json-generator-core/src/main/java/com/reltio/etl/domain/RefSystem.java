package com.reltio.etl.domain;

public class RefSystem {

	private String refEntitySourceSystem;
	private String refEntitySourceTable;

	private String refRelationSourceSystem;
	private String refRelationSourceTable;
	/**
	 * @return the refEntitySourceSystem
	 */
	public String getRefEntitySourceSystem() {
		return refEntitySourceSystem;
	}
	/**
	 * @param refEntitySourceSystem the refEntitySourceSystem to set
	 */
	public void setRefEntitySourceSystem(String refEntitySourceSystem) {
		this.refEntitySourceSystem = refEntitySourceSystem;
	}
	/**
	 * @return the refEntitySourceTable
	 */
	public String getRefEntitySourceTable() {
		return refEntitySourceTable;
	}
	/**
	 * @param refEntitySourceTable the refEntitySourceTable to set
	 */
	public void setRefEntitySourceTable(String refEntitySourceTable) {
		this.refEntitySourceTable = refEntitySourceTable;
	}
	/**
	 * @return the refRelationSourceSystem
	 */
	public String getRefRelationSourceSystem() {
		return refRelationSourceSystem;
	}
	/**
	 * @param refRelationSourceSystem the refRelationSourceSystem to set
	 */
	public void setRefRelationSourceSystem(String refRelationSourceSystem) {
		this.refRelationSourceSystem = refRelationSourceSystem;
	}
	/**
	 * @return the refRelationSourceTable
	 */
	public String getRefRelationSourceTable() {
		return refRelationSourceTable;
	}
	/**
	 * @param refRelationSourceTable the refRelationSourceTable to set
	 */
	public void setRefRelationSourceTable(String refRelationSourceTable) {
		this.refRelationSourceTable = refRelationSourceTable;
	}

	
}
