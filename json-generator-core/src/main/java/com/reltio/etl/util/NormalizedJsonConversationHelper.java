/**
 *
 */
package com.reltio.etl.util;

import static com.reltio.etl.constants.JsonGeneratorCoreProps.INTERACTION_CROSSWALK_VALUE;
import static com.reltio.etl.constants.JsonGeneratorCoreProps.INTERACTION_OBJECT_URI;
import static com.reltio.etl.constants.JsonGeneratorCoreProps.INTERACTION_SOURCE_SYSTEM;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;

import com.reltio.etl.constants.JsonGeneratorCoreProps;
import com.reltio.etl.domain.Attribute;
import com.reltio.etl.domain.Crosswalk;
import com.reltio.etl.domain.InteractionMember;
import com.reltio.etl.domain.MultiValueCrosswalk;
import com.reltio.etl.domain.NestedValue;
import com.reltio.etl.domain.ReferenceValue;
import com.reltio.etl.domain.RelationReference;

/**
 * This class is used to generate the JSON for Simple, Nested & Reference
 * Attributes
 */
public class NormalizedJsonConversationHelper {

    private static Map<String, Boolean> multivalueMap = new HashMap<>();

    /**
     * This method is responsible for creating complete attributes from Map to
     * Reltio
     *
     * @param attributes
     * @param attributeValues
     */
    @SuppressWarnings("unchecked")
    public static void createAttributes(Map<String, Collection<Object>> attributes, Map<String, Object> attributeValues,
                                        String[] lineValues, Map<String, Integer> columnIndexMap, Set<String> nonColumnValues, Crosswalk crosswalk,
                                        String columnDelimiter, boolean isNullIgnore, String nestedDelimeter) {

        Set<Object> processAttrs = null;

        if (attributeValues != null) {

            if (attributes == null) {
                attributes = new HashMap<String, Collection<Object>>();
            }

            // Iterate over all the list of attributes
            for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {

                processAttrs = (Set<Object>) attributes.get(inputValues.getKey());

                if (processAttrs == null) {
                    processAttrs = new HashSet<>();
                }

                // Check if it is Simple & Single Value Attribute
                if (inputValues.getValue() instanceof String) {
                    if (checkNotNull((String) inputValues.getValue())) {
                        String inputVal = (String) inputValues.getValue();
                        populateSimpleAttributes(inputVal, lineValues, processAttrs, columnDelimiter, isNullIgnore);
                        if (!processAttrs.isEmpty()) {
                            attributes.put(inputValues.getKey(), processAttrs);
                        }
                    }
                }

                // Check if it is multivalued
                if (inputValues.getValue() instanceof List) {
                    List<Object> allValues = (List<Object>) inputValues.getValue();
                    if (!allValues.isEmpty()) {
                        Object temp = allValues.get(0);

                        // Check if it is Multivalued & Simple
                        if (temp instanceof String) {
                            List<String> strValues = (List<String>) inputValues.getValue();
                            for (String inputVal : strValues) {
                                if (checkNotNull(inputVal)) {
                                    populateSimpleAttributes(inputVal, lineValues, processAttrs, columnDelimiter,
                                            isNullIgnore);
                                }
                            }

                            if (!processAttrs.isEmpty()) {
                                attributes.put(inputValues.getKey(), processAttrs);
                            }
                        } else if (temp instanceof Map) {

                            // If it is Multivalued & Nested/reference Attribute
                            List<Map<String, Object>> nestedValues = (List<Map<String, Object>>) inputValues.getValue();
                            for (Map<String, Object> innerNestedValues : nestedValues) {
                                handleNestedReferenceValues(innerNestedValues, attributes, inputValues.getKey(),
                                        lineValues, columnIndexMap, nonColumnValues, crosswalk, columnDelimiter, isNullIgnore, nestedDelimeter);
                            }
                        }
                    }
                } else if (inputValues.getValue() instanceof Map) {
                    // If it is Single Valued and Nested/Refernce Attributes
                    Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                    handleNestedReferenceValues(innerNestValues, attributes, inputValues.getKey(), lineValues,
                            columnIndexMap, nonColumnValues, crosswalk, columnDelimiter, isNullIgnore, nestedDelimeter);
                }

            }

        }

    }

    @SuppressWarnings("unchecked")
    public static void createMembers(Map<String, InteractionMember> members, Map<String, Object> memberValues,
                                     String[] lineValues, Map<String, Integer> columnIndexMap, Crosswalk crosswalk) {

        InteractionMember interactionMember = null;
        if (memberValues != null && !memberValues.isEmpty()) {

            if (members == null) {
                members = new HashMap<>();
            }

            // Iterate over all the list of members
            for (Map.Entry<String, Object> inputValues : memberValues.entrySet()) {

                if (inputValues.getValue() != null) {
                    interactionMember = members.get(inputValues.getKey());

                    if (interactionMember == null) {
                        interactionMember = new InteractionMember();
                    }

                    List<Map<String, String>> membersMap = (List<Map<String, String>>) inputValues.getValue();

                    for (Map<String, String> member : membersMap) {
                        createMember(member, lineValues, interactionMember, crosswalk);
                    }

                    if (interactionMember.getMembers() != null && !interactionMember.getMembers().isEmpty()) {
                        members.put(inputValues.getKey(), interactionMember);
                    }
                }

            }

        }

    }

    private static void createMember(Map<String, String> memberValues, String[] lineValues,
                                     InteractionMember interactionMember, Crosswalk crosswalk) {

        RelationReference relationReference = null;
        if (memberValues.get(INTERACTION_OBJECT_URI) != null && !memberValues.get(INTERACTION_OBJECT_URI).isEmpty()) {
            relationReference = new RelationReference();
            String value = lineValues[Integer.parseInt(memberValues.get(INTERACTION_OBJECT_URI))];

            if (checkNotNull(value)) {
                relationReference.setObjectURI(value);
                interactionMember.getMembers().add(relationReference);

            }
        } else if (memberValues.get(INTERACTION_CROSSWALK_VALUE) != null
                && !memberValues.get(INTERACTION_CROSSWALK_VALUE).isEmpty()) {

            relationReference = new RelationReference();
            String sourceSystem = memberValues.get(INTERACTION_SOURCE_SYSTEM);
            if (sourceSystem == null || sourceSystem.isEmpty()) {
                sourceSystem = crosswalk.value;
            }
            String value = lineValues[Integer.parseInt(memberValues.get(INTERACTION_CROSSWALK_VALUE))];

            if (checkNotNull(value)) {
                Crosswalk memberCrosswalk = new Crosswalk();
                memberCrosswalk.type = sourceSystem;
                memberCrosswalk.value = value;
                relationReference.getCrosswalks().add(memberCrosswalk);
                interactionMember.getMembers().add(relationReference);
            }

        }

    }

    /**
     * This method helps to populate the Simple Attributes from the Input column
     * Index and line Values array
     *
     * @param inputVal
     * @param lineValues
     * @param processAttrs
     * @param columnDelimiter
     */
    private static void populateSimpleAttributes(String inputVal, String[] lineValues, Set<Object> processAttrs,
                                                 String columnDelimiter, boolean isNullIgnore) {

        // Check whether it is a Static Value
        if (isStaticValue(inputVal)) {
            inputVal = getStaticMultiValue(inputVal);
            addAttributeToList(inputVal, processAttrs);
        } else if (isMultiValue(inputVal)) {
            // If it is a Multivalue then get the Column Name
            String columnName = getStaticMultiValue(inputVal);
            inputVal = getColumnValue(columnName, lineValues);
            String[] allInputValues = getAllValuesFromColumn(inputVal, columnDelimiter);
            addAttributeToList(allInputValues, processAttrs, isNullIgnore);
        } else {
            // If it is a single value column then get the value for that Column
            inputVal = getColumnValue(inputVal, lineValues);
            addAttributeToList(inputVal, processAttrs, isNullIgnore);
        }

    }

    /**
     * This is a helper method to split the values using the provided delimiter
     *
     * @param value
     * @param delimiter
     * @return
     */
    public static String[] getAllValuesFromColumn(String value, String delimiter) {
        String[] allValues = null;

        if (checkNotNull(value)) {
            allValues = value.split(delimiter, -1);
        }

        return allValues;
    }

    /**
     * This is a helper method to add the Column value to the List which stores
     * as a Reltio Attribute Object
     *
     * @param inputVal
     * @param processAttrs
     */
    private static void addAttributeToList(String inputVal, Set<Object> processAttrs, boolean isNullIgnore) {
        if (isNullIgnore) {
            if (checkNotNull(inputVal)) {
                Attribute attribute = new Attribute();
                if ("null".equalsIgnoreCase(inputVal.trim()) || "".equals(inputVal.trim())) {
                    attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                } else {
                    attribute.value = inputVal;
                }
                processAttrs.add(attribute);
            }
        } else {
            if (checkNotNull(inputVal) && !inputVal.trim().equals("") && !inputVal.toLowerCase().trim().equals("null")) {
                Attribute attribute = new Attribute();
                //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                attribute.value = inputVal;
                processAttrs.add(attribute);
            }
        }
    }

    private static void addAttributeToList(String inputVal, Set<Object> processAttrs) {
        if (checkNotNull(inputVal)) {
            Attribute attribute = new Attribute();
            attribute.value = inputVal;
            processAttrs.add(attribute);
        }
    }

    private static void addAttributeToList(String[] inputValues, Set<Object> processAttrs, boolean isNullIgnore) {
        if (inputValues != null) {
            for (String inputVal : inputValues) {
                addAttributeToList(inputVal, processAttrs, isNullIgnore);
            }
        }
    }

    /**
     * Checks whether the column is static Value Column
     *
     * @param inputVal
     * @return true/false
     */
    public static boolean isStaticValue(String inputVal) {
        if (inputVal == null) return false;
        return inputVal.contains("{") && inputVal.contains("}");
    }

    /**
     * Checks whether the input column is Multi value from Normalized File
     *
     * @param inputVal
     * @return
     */
    public static boolean isMultiValue(String inputVal) {
        return inputVal.startsWith("[") && inputVal.endsWith("]");
    }

    /**
     * This is the helper method to get the Column Value from whole line data
     * using the column Index
     *
     * @param columnIndex
     * @param lineValues
     * @return Column Value String
     */
    private static String getColumnValue(String columnIndex, String[] lineValues) {

        String value = lineValues[Integer.parseInt(columnIndex)];

        if (checkNotNull(value)) {
            return value.trim();
        }
        return null;

    }

    /**
     * Removes the First & last character from the String to get the
     * static/Multi Value Column Index
     *
     * @param staticVal
     * @return columnIndex
     */
    public static String getStaticMultiValue(String staticVal) {

        return staticVal.substring(1, staticVal.length() - 1);

    }

    /**
     * This helper method for de-normalized Nested Attribute Creation Attributes
     *
     * @param attributes
     * @param nestedAttributeName
     * @param attributeValues
     */
    @SuppressWarnings("unchecked")
    public static boolean createNestedAttributes(Map<String, Collection<Object>> attributes, String nestedAttributeName,
                                                 Map<String, Object> attributeValues, String[] lineValues, Map<String, Integer> columnIndexMap,
                                                 Set<String> nonColumnValues, Crosswalk crosswalk, boolean isNullIgnore) {

        Set<Object> processAttrs = null;
        List<Object> nestedAttrs = null;
        boolean isNonStaticExist = false;

        Map<String, Collection<Object>> innerNestedAttributes = new HashMap<>();

        if (attributes == null) {
            attributes = new HashMap<String, Collection<Object>>();
        }

        if (attributeValues != null && nestedAttributeName != null) {
            nestedAttrs = (List<Object>) attributes.get(nestedAttributeName);

            if (nestedAttrs == null) {
                nestedAttrs = new ArrayList<>();
            }

            // Iterate over all the sub attributes in the Nested Attribute
            for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {
                processAttrs = new HashSet<>();

                // Checks whether the sub attribute is Simple
                if (inputValues.getValue() instanceof String) {
                    if (JsonCreationHelper.checkNull((String) inputValues.getValue())) {

                        String inputVal = (String) inputValues.getValue();
                        if (isStaticValue(inputVal)) {
                            inputVal = getStaticMultiValue(inputVal);
                        } else {
                            inputVal = getColumnValue(inputVal, lineValues);
                            if (JsonCreationHelper.checkNull(inputVal)) {
                                isNonStaticExist = true;
                            }
                        }

                        if (isNullIgnore) {
                            if (checkNotNull(inputVal)) {
                                Attribute attribute = new Attribute();
                                if ("null".equalsIgnoreCase(inputVal.trim()) || "".equals(inputVal.trim())) {
                                    attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                } else {
                                    attribute.value = inputVal;
                                }
                                processAttrs.add(attribute);
                            }
                        } else {
                            if (JsonCreationHelper.checkNull(inputVal) && !inputVal.trim().equals("") && !inputVal.toLowerCase().trim().equals("null")) {
                                Attribute attribute = new Attribute();
                                //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                attribute.value = inputVal;
                                processAttrs.add(attribute);
                            }

                        }

                        if (!processAttrs.isEmpty()) {
                            innerNestedAttributes.put(inputValues.getKey(), processAttrs);

                        }
                    }
                }
                // Checks whether sub attribute Multi valued Simple
                if (inputValues.getValue() instanceof List) {
                    List<Object> allValues = (List<Object>) inputValues.getValue();
                    if (!allValues.isEmpty()) {

                        Object subAttrObj = allValues.get(0);
                        if (subAttrObj instanceof String) {
                            List<String> allValuesStr = (List<String>) inputValues.getValue();
                            for (String str : allValuesStr) {
                                if (JsonCreationHelper.checkNull(str)) {
                                    String inputVal = str;
                                    if (isStaticValue(inputVal)) {
                                        inputVal = getStaticMultiValue(inputVal);
                                    } else {
                                        inputVal = getColumnValue(inputVal, lineValues);
                                        if (JsonCreationHelper.checkNull(inputVal)) {
                                            isNonStaticExist = true;
                                        }
                                    }

                                    if (isNullIgnore) {
                                        if (checkNotNull(inputVal)) {
                                            Attribute attribute = new Attribute();
                                            if ("null".equalsIgnoreCase(inputVal.trim()) || "".equals(inputVal.trim())) {
                                                attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                            } else {
                                                attribute.value = inputVal;
                                            }
                                            processAttrs.add(attribute);
                                        }
                                    } else {
                                        if (JsonCreationHelper.checkNull(inputVal) && !inputVal.trim().equals("") && !inputVal.toLowerCase().trim().equals("null")) {
                                            Attribute attribute = new Attribute();
                                            //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                            attribute.value = inputVal;
                                            processAttrs.add(attribute);
                                        }

                                    }
                                }
                            }
                            if (!processAttrs.isEmpty()) {
                                innerNestedAttributes.put(inputValues.getKey(), processAttrs);
                            }
                        } else if (subAttrObj instanceof Map) {

                            List<Map<String, Object>> allValluesMap = (List<Map<String, Object>>) inputValues
                                    .getValue();
                            for (Map<String, Object> innerNestValues : allValluesMap) {
                                isNonStaticExist = createNestedAttributes(innerNestedAttributes, inputValues.getKey(), innerNestValues,
                                        lineValues, columnIndexMap, nonColumnValues, crosswalk, isNullIgnore);
                            }
                        }
                    }
                } else if (inputValues.getValue() instanceof Map) {
                    // Process if the sub attribute is again a Nested attribute
                    Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                    isNonStaticExist = createNestedAttributes(innerNestedAttributes, inputValues.getKey(), innerNestValues, lineValues,
                            columnIndexMap, nonColumnValues, crosswalk, isNullIgnore);

                }
            }
        }

        // Checks whether at least one value exist in the sub attributes and
        // also it is non-static
        if (!innerNestedAttributes.isEmpty() && isNonStaticExist) {
            NestedValue nestedValue = new NestedValue();
            nestedValue.value = innerNestedAttributes;
            nestedAttrs.add(nestedValue);
            attributes.put(nestedAttributeName, nestedAttrs);
        }

        return isNonStaticExist;
    }

    /**
     * This is helper method for generating normalized Nested attributes
     *
     * @param attributes
     * @param nestedAttributeName
     * @param attributeValues
     * @param lineValues
     * @param columnIndexMap
     * @param nonColumnValues
     * @param crosswalk
     * @param multiValueIndex
     * @return
     */
    @SuppressWarnings("unchecked")
    private static boolean createNormalizedNestedAttributes(Map<String, Collection<Object>> attributes,
                                                            String nestedAttributeName, Map<String, Object> attributeValues, String[] lineValues,
                                                            Map<String, Integer> columnIndexMap, Set<String> nonColumnValues, Crosswalk crosswalk,
                                                            Integer multiValueIndex, boolean isNullIgnore, int sizeOfMultiValue, String nestedDelimeter) {

        List<Object> processAttrs = null;
        List<Object> nestedAttrs = null;
        boolean isNonStaticExist = false;

        Map<String, Collection<Object>> innerNestedAttributes = new HashMap<>();

        if (attributes == null) {
            attributes = new HashMap<String, Collection<Object>>();
        }

        if (attributeValues != null && nestedAttributeName != null) {
            nestedAttrs = (List<Object>) attributes.get(nestedAttributeName);

            if (nestedAttrs == null) {
                nestedAttrs = new ArrayList<>();
            }

            // Iterate over all the sub attributes
            for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {
                processAttrs = new ArrayList<>();
                // Checks whether it is simple static value
                if (inputValues.getValue() instanceof String) {
                    String inputValue = (String) inputValues.getValue();
                    if (isNullIgnore) {
                        if (checkNotNull(inputValue)) {
                            Attribute attribute = new Attribute();
                            if ("null".equalsIgnoreCase(inputValue.trim()) || "".equals(inputValue.trim())) {
                                attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                            } else {
                                attribute.value = inputValue;
                            }
                            processAttrs.add(attribute);
                        }
                    } else {
                        if (checkNotNull(inputValue) && !inputValue.trim().equals("") && !inputValue.toLowerCase().trim().equals("null")) {
                            Attribute attribute = new Attribute();
                            //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                            attribute.value = inputValue;
                            processAttrs.add(attribute);
                        }
                    }

                    if (!processAttrs.isEmpty()) {
                        innerNestedAttributes.put(inputValues.getKey(), processAttrs);

                    }

                }
                // Checks whether it is simple attribute with non-static value
                if (inputValues.getValue() instanceof String[]) {
                    String[] inputValueArray = (String[]) inputValues.getValue();

                    if (inputValueArray != null) {
                        if (isNullIgnore) {

                            if (inputValueArray.length > multiValueIndex) {


                                if (checkNotNull(inputValueArray[multiValueIndex])) {

                                    String value = inputValueArray[multiValueIndex];
                                    String[] values = (nestedDelimeter != null) ? value.split(nestedDelimeter, -1) : new String[1];

                                    if (values.length > 1) {
                                        for (String v : values) {
                                            Attribute attribute = new Attribute();

                                            if ("null".equalsIgnoreCase(v)
                                                    || "".equals(v)) {
                                                attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                            } else {
                                                attribute.value = v;
                                            }

                                            processAttrs.add(attribute);
                                        }

                                    } else {
                                        Attribute attribute = new Attribute();
                                        if ("null".equalsIgnoreCase(inputValueArray[multiValueIndex].trim())
                                                || "".equals(inputValueArray[multiValueIndex].trim())) {
                                            attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                        } else {
                                            attribute.value = inputValueArray[multiValueIndex];
                                        }
                                        processAttrs.add(attribute);

                                    }

                                }
                            } else {
                                continue;
                            }
                        } else {
                            if (inputValueArray.length > multiValueIndex) {

                                if (checkNotNull(inputValueArray[multiValueIndex]) && !inputValueArray[multiValueIndex].trim().equals("") && !inputValueArray[multiValueIndex].toLowerCase().trim().equals("null")) {
                                    //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                    String value = inputValueArray[multiValueIndex];
                                    String[] values = (nestedDelimeter != null) ? value.split(nestedDelimeter, -1) : new String[1];

                                    if (values.length > 1) {
                                        for (String v : values) {
                                        	if (JsonCreationHelper.checkNull(v) && !v.trim().equals("") && !v.toLowerCase().trim().equals("null")) {
                                            Attribute attribute = new Attribute();
                                            attribute.value = v;
                                            processAttrs.add(attribute);
                                        }
                                        }
                                    } else {
                                        Attribute attribute = new Attribute();
                                        attribute.value = value;
                                        processAttrs.add(attribute);
                                    }

                                }
                            } else {
                                continue;
                            }
                        }


                        if (!processAttrs.isEmpty()) {
                            isNonStaticExist = true;
                            innerNestedAttributes.put(inputValues.getKey(), processAttrs);

                        }
                    }
                }
                // Check whether it is Multivalue and simple/Nested
                else if (inputValues.getValue() instanceof List) {

                    List<Object> allValues = (List<Object>) inputValues.getValue();
                    if (!allValues.isEmpty()) {

                        Object subattrObj = allValues.get(0);
                        System.out.println(inputValues.getKey());
                        if (subattrObj instanceof String[]) {

                            List<String[]> allValuesStr = (List<String[]>) inputValues.getValue();
                            for (String[] str : allValuesStr) {
                                if (checkNotNull(str[multiValueIndex])) {
                                    String inputVal = (String) str[multiValueIndex];
                                    if (isNullIgnore) {
                                        if (checkNotNull(inputVal)) {
                                            Attribute attribute = new Attribute();
                                            if ("null".equalsIgnoreCase(inputVal.trim()) || "".equals(inputVal.trim())) {
                                                attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                            } else {
                                                attribute.value = inputVal;
                                            }
                                            processAttrs.add(attribute);
                                        }
                                    } else {
                                        if (checkNotNull(inputVal) && !inputVal.trim().equals("") && !inputVal.toLowerCase().trim().equals("null")) {
                                            Attribute attribute = new Attribute();
                                            //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                            attribute.value = inputVal;
                                            processAttrs.add(attribute);
                                        }
                                    }
                                }
                            }
                            if (!processAttrs.isEmpty()) {
                                isNonStaticExist = true;
                                innerNestedAttributes.put(inputValues.getKey(), processAttrs);
                            }
                        } else if (subattrObj instanceof Map) {
                            List<Map<String, Object>> allValuesMap = (List<Map<String, Object>>) inputValues.getValue();
                            for (Map<String, Object> innerNestValues : allValuesMap) {
                                for (int i = 0; i < sizeOfMultiValue; i++) {
                                    boolean isNonStatic = createNormalizedNestedAttributes(innerNestedAttributes,
                                            inputValues.getKey(), innerNestValues, lineValues, columnIndexMap,
                                            nonColumnValues, crosswalk, i, isNullIgnore, sizeOfMultiValue, nestedDelimeter);

                                    if (isNonStatic) {
                                        isNonStaticExist = isNonStatic;
                                    }

                                }

                            }
                        }
                    }
                }
                // Check whether the sub attribute again a nested attribute
                else if (inputValues.getValue() instanceof Map) {
                    Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                    if (multivalueMap.get(inputValues.getKey()) != null && multivalueMap.get(inputValues.getKey()) &&
                            (multivalueMap.get(nestedAttributeName) == null || !multivalueMap.get(nestedAttributeName))) {
                        for (int i = 0; i < sizeOfMultiValue; i++) {
                            boolean isNonStatic = createNormalizedNestedAttributes(innerNestedAttributes, inputValues.getKey(),
                                    innerNestValues, lineValues, columnIndexMap, nonColumnValues, crosswalk, i, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                            if (isNonStatic) {
                                isNonStaticExist = isNonStatic;
                            }
                        }

                    } else {

                        boolean isNonStatic = createNormalizedNestedAttributes(innerNestedAttributes, inputValues.getKey(),
                                innerNestValues, lineValues, columnIndexMap, nonColumnValues, crosswalk, multiValueIndex, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                        if (isNonStatic) {
                            isNonStaticExist = isNonStatic;
                        }
                    }


                }
            }
        }


        // Checks whether at least one value exist in the sub attributes and
        // also it is non-static
        if (!innerNestedAttributes.isEmpty() && isNonStaticExist) {
            List<Map<String, Collection<Object>>> innerNestedAttributesList = new ArrayList<>();
            boolean found = true;

            /*
             * This loop checks if the current node is transformed/converted to NestedValue already.
             * If No, next loop will segregate the multivalues if present to different  nodes
             */
            for (Map.Entry<String, Collection<Object>> entry : innerNestedAttributes.entrySet()) {

                if (entry.getValue().size() > 1) {
                    for (Object obj : entry.getValue()) {
                        if ((obj instanceof NestedValue)) {
                            found = false;
                        }

                    }
                } else {
                    found = false;
                }
            }


            if (found) {
                for (Map.Entry<String, Collection<Object>> entry : innerNestedAttributes.entrySet()) {

                    if (entry.getValue().size() > 1) {

                        int i = 0;
                        for (Object obj : entry.getValue()) {

                            if (obj instanceof Attribute) {
                                Map<String, Collection<Object>> innerNest = new HashMap<>();

                                if (innerNestedAttributesList.size() > i) {
                                    if (innerNestedAttributesList.get(i).get(entry.getKey()) != null) {
                                        innerNestedAttributesList.get(i).get(entry.getKey()).add(obj);
                                    } else {
                                        Set<Object> attrs = new HashSet<>();
                                        attrs.add(obj);
                                        innerNestedAttributesList.get(i).put(entry.getKey(), attrs);
                                    }
                                } else {
                                    Set<Object> attrs = new HashSet<>();
                                    attrs.add(obj);
                                    innerNest.put(entry.getKey(), attrs);
                                    innerNestedAttributesList.add(innerNest);
                                }
                                i++;
                            } else {
                                innerNestedAttributesList.add(innerNestedAttributes);
                                break;
                            }
                        }

                    } else {
                        Map<String, Collection<Object>> innerNest = new HashMap<>();
                        innerNest.put(entry.getKey(), entry.getValue());
                        innerNestedAttributesList.add(innerNest);
                    }


                }

            } else {
                innerNestedAttributesList.add(innerNestedAttributes);
            }


            for (Map<String, Collection<Object>> nest : innerNestedAttributesList) {
                NestedValue nestedValue = new NestedValue();
                nestedValue.value = nest;
                nestedAttrs.add(nestedValue);
            }
            attributes.put(nestedAttributeName, nestedAttrs);
        }

        return isNonStaticExist;
    }

    /**
     * This method helps to convert the Nested attribute with column index to
     * nested attribute with normalized values. The new map generated by this
     * method used for further processing normalized nested attribute
     *
     * @param attributeValues
     * @param lineValues
     * @param columnDelimiter
     * @param sizeOfMultiValue
     * @return
     */
    @SuppressWarnings("unchecked")
    private static Map<String, Object> getNormalizedAttributeValueMap(Map<String, Object> attributeValues,
                                                                      String[] lineValues, String columnDelimiter, Integer sizeOfMultiValue) {

        Map<String, Object> attributeValuesNewMap = new HashMap<>();

        for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {
            // Checks whether it is simple attribute
            if (inputValues.getValue() instanceof String) {
                if (checkNotNull((String) inputValues.getValue())) {

                    String inputVal = (String) inputValues.getValue();

                    getNormalizedAttributeArray(inputVal, lineValues, columnDelimiter, sizeOfMultiValue,
                            attributeValuesNewMap, inputValues.getKey());
                }
            }
            // Checks whether it is multi-value attribute
            else if (inputValues.getValue() instanceof List) {

                List<Object> allValues = (List<Object>) inputValues.getValue();

                if (!allValues.isEmpty()) {
                    Object subAttrObj = allValues.get(0);
                    if (subAttrObj instanceof String) {
                        List<String> allValuesStr = (List<String>) inputValues.getValue();

                        List<String[]> allValuesArray = new ArrayList<>();
                        for (String str : allValuesStr) {
                            if (checkNotNull(str)) {
                                String inputVal = str;
                                getNormalizedAttributeArray(inputVal, lineValues, columnDelimiter, sizeOfMultiValue,
                                        attributeValuesNewMap, inputValues.getKey());
                            }
                        }
                        attributeValuesNewMap.put(inputValues.getKey(), allValuesArray);
                    } else if (subAttrObj instanceof Map) {
                        List<Map<String, Object>> allValuesMap = (List<Map<String, Object>>) inputValues.getValue();
                        for (Map<String, Object> innerNestValues : allValuesMap) {
                            attributeValuesNewMap.put(inputValues.getKey(), getNormalizedAttributeValueMap(
                                    innerNestValues, lineValues, columnDelimiter, sizeOfMultiValue));

                        }
                    }

                }

            }
            // Checks whether it is nested sub attribute
            else if (inputValues.getValue() instanceof Map) {
                Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                attributeValuesNewMap.put(inputValues.getKey(),
                        getNormalizedAttributeValueMap(innerNestValues, lineValues, columnDelimiter, sizeOfMultiValue));

            }
            // Checks whether it represents any Entity/Relation crosswalk
            else if (inputValues.getValue() instanceof Crosswalk) {

                Crosswalk inputCrosswalk = (Crosswalk) inputValues.getValue();
                String inputValue = null;
                String[] valuesArray = null;

                // The below processing is to understand the crosswalk value
                // column is Static/non-static value which helps the further
                // processing
                if (isStaticValue(inputCrosswalk.value)) {
                    inputValue = getStaticMultiValue(inputCrosswalk.value);
                    Crosswalk crosswalk = new Crosswalk();
                    crosswalk.type = inputCrosswalk.type;
                    crosswalk.value = inputValue;

                    if (inputCrosswalk.sourceTable != null) {
                        crosswalk.sourceTable = inputCrosswalk.sourceTable;
                    }

                    attributeValuesNewMap.put(inputValues.getKey(), crosswalk);

                } else if (isMultiValue(inputCrosswalk.value)) {
                    String columnName = getStaticMultiValue(inputCrosswalk.value);
                    inputValue = getColumnValue(columnName, lineValues);
                    if (checkNotNull(inputValue)) {
                        valuesArray = inputValue.split(columnDelimiter, -1);
                    } else {
                        populateNormalizedArray(valuesArray, inputValue, sizeOfMultiValue);
                    }

                    MultiValueCrosswalk multiValueCrosswalk = new MultiValueCrosswalk();
                    multiValueCrosswalk.type = inputCrosswalk.type;
                    multiValueCrosswalk.value = valuesArray;

                    attributeValuesNewMap.put(inputValues.getKey(), multiValueCrosswalk);

                } else {
                    inputValue = getColumnValue(inputCrosswalk.value, lineValues);
                    Crosswalk crosswalk = new Crosswalk();
                    crosswalk.type = inputCrosswalk.type;
                    crosswalk.value = inputValue;
                    if (inputCrosswalk.sourceTable != null) {
                        crosswalk.sourceTable = inputCrosswalk.sourceTable;
                    }

                    attributeValuesNewMap.put(inputValues.getKey(), crosswalk);
                }

            }

        }

        return attributeValuesNewMap;
    }

    /**
     * This helper method to identify the Size of the Normalized Nested
     * attribute from the input File Column which helps on processing each value
     * properly in the process
     *
     * @param attributeValues
     * @param lineValues
     * @param columnDelimiter
     * @return
     */
    @SuppressWarnings("unchecked")
    private static Integer getNormalizedNestedAttributeValueSize(Map<String, Object> attributeValues,
                                                                 String[] lineValues, String columnDelimiter) {
        // Variable to store size of Normalized Column Values
        Integer sizeOfNormalizedValue = null;
        Integer largestValue = 0;

        for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {
            if (inputValues.getValue() instanceof String) {
                if (checkNotNull((String) inputValues.getValue())) {

                    String inputVal = (String) inputValues.getValue();

                    sizeOfNormalizedValue = getNormalizedColumnValueSize(inputVal, lineValues, columnDelimiter);
                    try {
                        if (sizeOfNormalizedValue > largestValue) {
                            largestValue = sizeOfNormalizedValue;
                        }
                    } catch (NullPointerException ignored) {

                    }
                    //                    if (sizeOfNormalizedValue != null) {
                    //                        break;
                    //                    }
                }
            }
            if (inputValues.getValue() instanceof List) {
                List<Object> allValues = (List<Object>) inputValues.getValue();

                if (!allValues.isEmpty()) {
                    Object subAttrObj = allValues.get(0);
                    if (subAttrObj instanceof String) {
                        List<String> allValuesStr = (List<String>) inputValues.getValue();
                        for (String str : allValuesStr) {
                            if (checkNotNull(str)) {
                                sizeOfNormalizedValue = getNormalizedColumnValueSize(str, lineValues,
                                        columnDelimiter);
                                try {
                                    if (sizeOfNormalizedValue > largestValue) {
                                        largestValue = sizeOfNormalizedValue;
                                    }
                                } catch (NullPointerException ignored) {
                                }
                                //                                if (sizeOfNormalizedValue != null) {
                                //                                    break;
                                //                                }
                            }
                        }
                    } else if (subAttrObj instanceof Map) {
                        List<Map<String, Object>> allValuesMap = (List<Map<String, Object>>) inputValues.getValue();
                        for (Map<String, Object> innerNestValues : allValuesMap) {
                            sizeOfNormalizedValue = getNormalizedNestedAttributeValueSize(innerNestValues, lineValues,
                                    columnDelimiter);
                            try {
                                if (sizeOfNormalizedValue > largestValue) {
                                    largestValue = sizeOfNormalizedValue;
                                }
                            } catch (NullPointerException ignored) {
                            }
                            //                            if (sizeOfNormalizedValue != null) {
                            //                                break;
                            //                            }

                        }

                    }
                }
            } else if (inputValues.getValue() instanceof Map) {
                Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                sizeOfNormalizedValue = getNormalizedNestedAttributeValueSize(innerNestValues, lineValues,
                        columnDelimiter);
                try {
                    if (sizeOfNormalizedValue > largestValue) {
                        largestValue = sizeOfNormalizedValue;
                    }
                } catch (NullPointerException ignored) {
                }
                //                if (sizeOfNormalizedValue != null) {
                //                    break;
                //                }

            }
            //            if (sizeOfNormalizedValue != null) {
            //                break;
            //            }

        }

        return largestValue;
    }

    /**
     * This helper method to identify the Size of the Normalized Nested
     * attribute from the input File Column which helps on processing each value
     * properly in the process
     *
     * @param attributeValues
     * @param lineValues
     * @param columnDelimiter
     * @return
     */
    @SuppressWarnings("unchecked")
    private static Boolean isNormalizedNestedRefAttribute(Map<String, Object> attributeValues, String[] lineValues,
                                                          String columnDelimiter) {
        // Variable to store size of Normalized Column Values
        Boolean isNormalized = false;

        for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {
            if (inputValues.getValue() instanceof String) {
                if (checkNotNull((String) inputValues.getValue())) {

                    String inputVal = (String) inputValues.getValue();
                    isNormalized = isMultiValue(inputVal);

                    if (isNormalized) {
                        break;
                    }
                }
            }
            if (inputValues.getValue() instanceof List) {
                List<Object> allValues = (List<Object>) inputValues.getValue();

                if (!allValues.isEmpty()) {
                    Object subAttrObj = allValues.get(0);
                    if (subAttrObj instanceof String) {
                        List<String> allValuesStr = (List<String>) inputValues.getValue();
                        for (String str : allValuesStr) {
                            if (checkNotNull(str)) {
                                String inputVal = str;
                                isNormalized = isMultiValue(inputVal);

                                if (isNormalized) {
                                    break;
                                }
                            }
                        }
                    } else if (subAttrObj instanceof Map) {
                        List<Map<String, Object>> allValuesMap = (List<Map<String, Object>>) inputValues.getValue();
                        for (Map<String, Object> innerNestValues : allValuesMap) {
                            isNormalized = isNormalizedNestedRefAttribute(innerNestValues, lineValues, columnDelimiter);
                            if (isNormalized) {
                                break;
                            }

                        }

                    }
                }
            } else if (inputValues.getValue() instanceof Map) {
                Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                isNormalized = isNormalizedNestedRefAttribute(innerNestValues, lineValues, columnDelimiter);
                if (isNormalized) {
                    break;
                }

            }
            if (isNormalized) {
                break;
            }

        }

        return isNormalized;
    }

    /**
     * This helper method used to convert the map of
     * <AttributeName,ColumnIndex> to <AttributeName,String Array>. ColumnIndex
     * ==> Column Value ==> Split ==> String array
     *
     * @param inputVal
     * @param lineValues
     * @param columnDelimiter
     * @param sizeOfMultiValue
     * @param attributeValuesNewMap
     * @param attributeName
     */
    private static void getNormalizedAttributeArray(String inputVal, String[] lineValues, String columnDelimiter,
                                                    Integer sizeOfMultiValue, Map<String, Object> attributeValuesNewMap, String attributeName) {

        String[] valuesArray = new String[sizeOfMultiValue];
        String inputValue;

        if (isStaticValue(inputVal)) {
            inputValue = getStaticMultiValue(inputVal);
            attributeValuesNewMap.put(attributeName, inputValue);
        } else if (isMultiValue(inputVal)) {
            String columnName = getStaticMultiValue(inputVal);
            inputValue = getColumnValue(columnName, lineValues);
            if (checkNotNull(inputValue)) {
                valuesArray = inputValue.split(columnDelimiter, -1);
            } else {
                populateNormalizedArray(valuesArray, inputValue, sizeOfMultiValue);
            }
            attributeValuesNewMap.put(attributeName, valuesArray);

        } else if (multivalueMap.get(attributeName) != null && multivalueMap.get(attributeName)) {
            inputValue = getColumnValue(inputVal, lineValues);
            populateNormalizedArray(valuesArray, inputValue, sizeOfMultiValue);
            attributeValuesNewMap.put(attributeName, valuesArray);
        } else {
            inputValue = getColumnValue(inputVal, lineValues);
            attributeValuesNewMap.put(attributeName, inputValue);
        }

    }

    /**
     * This helper method used to populate the single value in all array
     * elements
     *
     * @param valuesArray
     * @param inputValue
     * @param sizeOfMultiValue
     */
    private static void populateNormalizedArray(String[] valuesArray, String inputValue, Integer sizeOfMultiValue) {
        for (int i = 0; i < sizeOfMultiValue; i++) {
            valuesArray[i] = inputValue;
        }

    }

    /**
     * This is helper method to find out the number of values present in the
     * normalized column using the column delimiter
     *
     * @param inputVal
     * @param lineValues
     * @param columnDelimiter
     * @return Size of Normalized Column Value
     */
    private static Integer getNormalizedColumnValueSize(String inputVal, String[] lineValues, String columnDelimiter) {

        Integer sizeOfNormalizedColValue = null;
        if (isMultiValue(inputVal)) {
            String columnName = getStaticMultiValue(inputVal);
            inputVal = getColumnValue(columnName, lineValues);

            if (checkNotNull(inputVal))
                sizeOfNormalizedColValue = inputVal.split(columnDelimiter, -1).length;

        }

        return sizeOfNormalizedColValue;

    }

    /**
     *
     * This is helper method to convert the de-normalized nested file columns to
     * Reltio attributes
     *
     * @param attributes
     * @param nestedAttributeName
     * @param attributeValues
     */
    @SuppressWarnings("unchecked")
    private static boolean createReferenceAttributes(Map<String, Collection<Object>> attributes,
                                                     String nestedAttributeName, Map<String, Object> attributeValues, String[] lineValues,
                                                     Map<String, Integer> columnIndexMap, Set<String> nonColumnValues, Crosswalk crosswalk, boolean isNullIgnore) {

        Set<Object> processAttrs = null;
        List<Object> refAttrs = null;
        boolean isNonStaticExist = false;

        Map<String, Collection<Object>> innerReferenceAttributes = new HashMap<>();
        if (attributes == null) {
            attributes = new HashMap<String, Collection<Object>>();
        }

        if (attributeValues != null && nestedAttributeName != null) {
            refAttrs = (List<Object>) attributes.get(nestedAttributeName);

            if (refAttrs == null) {
                refAttrs = new ArrayList<>();
            }

            // Iterate over all the Sub attributes
            for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {

                // Skips the Ref Entity & Ref Relation part as these are not an
                // attributes
                if (!inputValues.getKey().equalsIgnoreCase("refEntity")
                        && !inputValues.getKey().equalsIgnoreCase("refRelation")) {
                    processAttrs = new HashSet<>();

                    // Checks whether it is a simple attribute
                    if (inputValues.getValue() instanceof String) {
                        String inputVal = (String) inputValues.getValue();

                        if (JsonCreationHelper.checkNull(inputVal)) {

                            if (isStaticValue(inputVal)) {
                                inputVal = getStaticMultiValue(inputVal);
                            } else {
                                inputVal = getColumnValue(inputVal, lineValues);
                                if (JsonCreationHelper.checkNull(inputVal)) {
                                    isNonStaticExist = true;
                                }
                            }

                            if (isNullIgnore) {

                                if (checkNotNull(inputVal)) {
                                    Attribute attribute = new Attribute();
                                    if ("null".equalsIgnoreCase(inputVal.trim()) || "".equals(inputVal.trim())) {
                                        attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                    } else {
                                        attribute.value = inputVal;
                                    }
                                    processAttrs.add(attribute);
                                }
                            } else {
                                if (JsonCreationHelper.checkNull(inputVal) && !inputVal.trim().equals("")
                                        && !inputVal.toLowerCase().trim().equals("null")) {
                                    Attribute attribute = new Attribute();
                                    // attribute.value =
                                    // JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                    attribute.value = inputVal;
                                    processAttrs.add(attribute);
                                }
                            }

                            if (!processAttrs.isEmpty()) {
                                innerReferenceAttributes.put(inputValues.getKey(), processAttrs);
                            }
                        }

                    }
                    // Checks whether is a Multivalue attribute
                    else if (inputValues.getValue() instanceof List) {
                        List<Object> allValues = (List<Object>) inputValues.getValue();

                        if (!allValues.isEmpty()) {

                            Object subAttrObj = allValues.get(0);
                            if (subAttrObj instanceof String) {
                                List<String> allValuesStr = (List<String>) inputValues.getValue();

                                for (String str : allValuesStr) {
                                    if (JsonCreationHelper.checkNull(str)) {
                                        String inputVal = str;
                                        if (isStaticValue(inputVal)) {
                                            inputVal = getStaticMultiValue(inputVal);
                                        } else {
                                            inputVal = getColumnValue(inputVal, lineValues);
                                            if (JsonCreationHelper.checkNull(inputVal)) {
                                                isNonStaticExist = true;
                                            }
                                        }

                                        if (isNullIgnore) {
                                            if (checkNotNull(inputVal)) {
                                                Attribute attribute = new Attribute();
                                                if ("null".equalsIgnoreCase(inputVal.trim()) || "".equals(inputVal.trim())) {
                                                    attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                                } else {
                                                    attribute.value = inputVal;
                                                }
                                                processAttrs.add(attribute);
                                            }
                                        } else {
                                            if (JsonCreationHelper.checkNull(inputVal) && !inputVal.trim().equals("") && !inputVal.toLowerCase().trim().equals("null")) {
                                                Attribute attribute = new Attribute();
                                                //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                                attribute.value = inputVal;
                                                processAttrs.add(attribute);
                                            }

                                        }
                                    }
                                }
                                if (!processAttrs.isEmpty()) {
                                    innerReferenceAttributes.put(inputValues.getKey(), processAttrs);
                                }
                            } else if (subAttrObj instanceof Map) {
                                List<Map<String, Object>> allValuesMap = (List<Map<String, Object>>) inputValues
                                        .getValue();
                                for (Map<String, Object> innerNestValues : allValuesMap) {
                                    isNonStaticExist = createNestedAttributes(innerReferenceAttributes,
                                            inputValues.getKey(), innerNestValues, lineValues, columnIndexMap,
                                            nonColumnValues, crosswalk, isNullIgnore);
                                }

                            }
                        }
                    }
                    // Checks whether it is Nested Sub attribute
                    else if (inputValues.getValue() instanceof Map) {
                        Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();
                        isNonStaticExist = createNestedAttributes(innerReferenceAttributes, inputValues.getKey(),
                                innerNestValues, lineValues, columnIndexMap, nonColumnValues, crosswalk, isNullIgnore);

                    }
                }
            }
        }

        // Checks whether at least one value exist in the sub attributes and
        // also it is non-static
        if (!innerReferenceAttributes.isEmpty() && isNonStaticExist) {
            ReferenceValue referenceValue = new ReferenceValue();
            referenceValue.value = innerReferenceAttributes;
            RelationReference refEntity = new RelationReference();
            RelationReference refRelation = new RelationReference();
            Crosswalk refEntityCross = (Crosswalk) attributeValues.get("refEntity");
            Crosswalk refRelationCross = (Crosswalk) attributeValues.get("refRelation");
            boolean isRefEntityStatic = false;
            Crosswalk refEntityCrossNew = new Crosswalk();

            if (refEntityCross != null) {
                refEntityCrossNew.type = crosswalk.type;

                String inputVal = refEntityCross.value;
                if (isStaticValue(inputVal)) {
                    inputVal = getStaticMultiValue(inputVal);
                    isRefEntityStatic = true;
                } else {
                    inputVal = getColumnValue(inputVal, lineValues);
                }
                
                if (refEntityCross.sourceTable != null) {
                    refEntityCrossNew.sourceTable = refEntityCross.sourceTable;
                }

                
                
                refEntityCrossNew.value = inputVal;
                refEntity.getCrosswalks().add(refEntityCrossNew);
                referenceValue.refEntity = refEntity;
            }

            Object refRelationObj = attributeValues.get("refRelation");
            Crosswalk refRelationCrosswalk = new Crosswalk();
            Crosswalk refReferenceCrossNew = new Crosswalk();

            
            if (refRelationObj != null) {

                if (refEntityCross instanceof Crosswalk) {
                    refRelationCrosswalk = (Crosswalk) refRelationObj;
                    if (refRelationCrosswalk != null) {

                    	
                        String inputVal = refRelationCrosswalk.value;
                        if (isStaticValue(inputVal)) {
                            inputVal = getStaticMultiValue(inputVal);
                            isRefEntityStatic = true;
                        } else {
                            inputVal = getColumnValue(inputVal, lineValues);
                        }


                        if (refRelationCrosswalk.type != null) {
                        	refReferenceCrossNew.type = refRelationCrosswalk.type;
                        } else {
                        	refReferenceCrossNew.type = crosswalk.type;
                        }

                        if (refRelationCrosswalk.sourceTable != null) {
                        	refReferenceCrossNew.sourceTable = refRelationCrosswalk.sourceTable;
                        }

                        refReferenceCrossNew.value = inputVal;

                    }

                }
            } else {
                if (crosswalk.value != null) {
                    refRelationCrosswalk.value = "R" + crosswalk.value.trim() + nestedAttributeName;
                } else {
                    refRelationCrosswalk.value = "R"
                            + RandomStringUtils.randomAlphanumeric(JsonGeneratorCoreProps.SIZE_OF_RANDOM_STRING - 3)
                            + nestedAttributeName;
                }
                if (isRefEntityStatic) {
                    refRelationCrosswalk.value += RandomStringUtils
                            .randomAlphanumeric(JsonGeneratorCoreProps.SIZE_OF_RANDOM_STRING);
                } else {
                    refRelationCrosswalk.value += refEntityCrossNew.value;
                }
                refRelationCrosswalk.type=crosswalk.type;
            }
            
            refRelation.getCrosswalks().add(refReferenceCrossNew);
            referenceValue.refRelation = refRelation;

            refAttrs.add(referenceValue);
            attributes.put(nestedAttributeName, refAttrs);
        }

        return isNonStaticExist;

    }

    /***
     *
     * This method will convert Normalized reference columns from input file to
     * Reltio attributes
     *
     * @param attributes
     * @param nestedAttributeName
     * @param attributeValues
     * @param lineValues
     * @param columnIndexMap
     * @param nonColumnValues
     * @param crosswalk
     * @param multiValueIndex
     * @return
     */
    @SuppressWarnings("unchecked")
    private static boolean createNormalizedReferenceAttributes(Map<String, Collection<Object>> attributes,
                                                               String nestedAttributeName, Map<String, Object> attributeValues, String[] lineValues,
                                                               Map<String, Integer> columnIndexMap, Set<String> nonColumnValues, Crosswalk crosswalk,
                                                               Integer multiValueIndex, boolean isNullIgnore, int sizeOfMultiValue, String nestedDelimeter) {

        Set<Object> processAttrs = null;
        List<Object> refAttrs = null;
        boolean isNonStaticExist = false;

        Map<String, Collection<Object>> innerReferenceAttributes = new HashMap<>();
        if (attributes == null) {
            attributes = new HashMap<String, Collection<Object>>();
        }

        if (attributeValues != null && nestedAttributeName != null) {
            refAttrs = (List<Object>) attributes.get(nestedAttributeName);

            if (refAttrs == null) {
                refAttrs = new ArrayList<>();
            }

            // Iterate over all sub attributes
            for (Map.Entry<String, Object> inputValues : attributeValues.entrySet()) {
                // Skip the crosswalk values like Refentity & RefRelation as
                // these are not attributes
                if (!inputValues.getKey().equalsIgnoreCase("refEntity")
                        && !inputValues.getKey().equalsIgnoreCase("refRelation")) {
                    processAttrs = new HashSet<>();

                    // Checks whether it is a static value column
                    if (inputValues.getValue() instanceof String) {
                        String inputValue = (String) inputValues.getValue();
                        if (isNullIgnore) {
                            if (checkNotNull(inputValue)) {
                                Attribute attribute = new Attribute();
                                if ("null".equalsIgnoreCase(inputValue.trim()) || "".equals(inputValue.trim())) {
                                    attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                } else {
                                    attribute.value = inputValue;
                                }
                                processAttrs.add(attribute);
                            }
                        } else {

                            {
                                if (checkNotNull(inputValue) && !inputValue.trim().equals("") && !inputValue.toLowerCase().trim().equals("null")) {
                                    Attribute attribute = new Attribute();
                                    //attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                    attribute.value = inputValue;
                                    processAttrs.add(attribute);
                                }

                            }
                        }
                        if (!processAttrs.isEmpty()) {
                            innerReferenceAttributes.put(inputValues.getKey(), processAttrs);

                        }

                    }
                    // Checks whether if it is non-static normalized value
                    else if (inputValues.getValue() instanceof String[]) {
                        String[] inputValueArray = (String[]) inputValues.getValue();

                        if (inputValueArray != null) {

                            if (isNullIgnore) {
                                if (inputValueArray.length > multiValueIndex) {

                                    if (checkNotNull(inputValueArray[multiValueIndex])) {
                                        String value = inputValueArray[multiValueIndex];
                                        String[] values = (nestedDelimeter != null) ? value.split(nestedDelimeter, -1) : new String[1];

                                        if (values.length > 1) {
                                            for (String v : values) {
                                                Attribute attribute = new Attribute();

                                                if ("null".equalsIgnoreCase(v)
                                                        || "".equals(v)) {
                                                    attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                                } else {
                                                    attribute.value = v;
                                                }

                                                processAttrs.add(attribute);
                                            }

                                        } else {
                                            Attribute attribute = new Attribute();
                                            if ("null".equalsIgnoreCase(inputValueArray[multiValueIndex].trim())
                                                    || "".equals(inputValueArray[multiValueIndex].trim())) {
                                                attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                            } else {
                                                attribute.value = inputValueArray[multiValueIndex];
                                            }
                                            processAttrs.add(attribute);
                                        }

                                    }
                                } else {
                                    continue;
                                }
                            } else {
                            	
                                if (inputValueArray.length > multiValueIndex) {
                                    if (checkNotNull(inputValueArray[multiValueIndex]) && !inputValueArray[multiValueIndex].trim().equals("") && !inputValueArray[multiValueIndex].toLowerCase().trim().equals("null")) {
                                        String value = inputValueArray[multiValueIndex];
                                        String[] values = (nestedDelimeter != null) ? value.split(nestedDelimeter, -1) : new String[1];


                                        if (values.length > 1) {
                                            for (String v : values) {
                                                Attribute attribute = new Attribute();
                                                attribute.value = v;
                                                processAttrs.add(attribute);
                                            }
                                        } else {
                                            Attribute attribute = new Attribute();
                                            attribute.value = value;
                                            processAttrs.add(attribute);
                                        }

                                    }
                               }else {
                            	   continue;
                               }


                            }

                            if (!processAttrs.isEmpty()) {
                                isNonStaticExist = true;
                                innerReferenceAttributes.put(inputValues.getKey(), processAttrs);

                            }
                        }

                    }
                    // Checks whether it is multi value attribute
                    else if (inputValues.getValue() instanceof List) {
                        List<Object> allValues = (List<Object>) inputValues.getValue();

                        if (!allValues.isEmpty()) {
                            Object subAttrObj = allValues.get(0);
                            if (subAttrObj instanceof String[]) {

                                List<String[]> allValuesStr = (List<String[]>) inputValues.getValue();
                                for (String[] str : allValuesStr) {

                                    if (checkNotNull(str[multiValueIndex])) {
                                        String inputVal = (String) str[multiValueIndex];
                                        if (isNullIgnore) {
                                            if (checkNotNull(inputVal)) {
                                                Attribute attribute = new Attribute();
                                                if ("null".equalsIgnoreCase(inputVal.trim())
                                                        || "".equals(inputVal.trim())) {
                                                    attribute.value = JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                                } else {
                                                    attribute.value = inputVal;
                                                }
                                                processAttrs.add(attribute);
                                            }
                                        } else {

                                            if (checkNotNull(inputVal) && !inputVal.trim().equals("")
                                                    && !inputVal.toLowerCase().trim().equals("null")) {
                                                Attribute attribute = new Attribute();
                                                // attribute.value =
                                                // JsonGeneratorCoreProps.NULL_PLACEHOLDER_VALUE;
                                                attribute.value = inputVal;
                                                processAttrs.add(attribute);
                                            }

                                        }
                                    }
                                }
                                if (!processAttrs.isEmpty()) {
                                    isNonStaticExist = true;
                                    innerReferenceAttributes.put(inputValues.getKey(), processAttrs);
                                }
                            } else if (subAttrObj instanceof Map) {
                                List<Map<String, Object>> allValuesMap = (List<Map<String, Object>>) inputValues
                                        .getValue();
                                for (Map<String, Object> innerNestValues : allValuesMap) {
                                    boolean isNonStatic = createNormalizedNestedAttributes(innerReferenceAttributes,
                                            inputValues.getKey(), innerNestValues, lineValues, columnIndexMap,
                                            nonColumnValues, crosswalk, multiValueIndex, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                                    if (isNonStatic) {
                                        isNonStaticExist = isNonStatic;
                                    }
                                }
                            }

                        }
                    }
                    // Checks whether it is nested sub attribute
                    else if (inputValues.getValue() instanceof Map) {
                        Map<String, Object> innerNestValues = (Map<String, Object>) inputValues.getValue();

                        if (multivalueMap.get(inputValues.getKey()) != null && multivalueMap.get(inputValues.getKey()) &&
                                (multivalueMap.get(nestedAttributeName) == null || !multivalueMap.get(nestedAttributeName))) {
                            for (int i = 0; i < sizeOfMultiValue; i++) {
                                boolean isNonStatic = createNormalizedNestedAttributes(innerReferenceAttributes, inputValues.getKey(),
                                        innerNestValues, lineValues, columnIndexMap, nonColumnValues, crosswalk, i, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                                if (isNonStatic) {
                                    isNonStaticExist = isNonStatic;
                                }
                            }

                        } else {
                            boolean isNonStatic = createNormalizedNestedAttributes(innerReferenceAttributes, inputValues.getKey(),
                                    innerNestValues, lineValues, columnIndexMap, nonColumnValues, crosswalk, multiValueIndex, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                            if (isNonStatic) {
                                isNonStaticExist = isNonStatic;
                            }
                        }


                    }
                }
            }
        }

        // Checks whether at least one value exist in the sub attributes and
        // also it is non-static
        if (!innerReferenceAttributes.isEmpty() && isNonStaticExist) {
            ReferenceValue referenceValue = new ReferenceValue();
            referenceValue.value = innerReferenceAttributes;
            RelationReference refEntity = new RelationReference();
            RelationReference refRelation = new RelationReference();

            Object refEntitiyObj = attributeValues.get("refEntity");
            Crosswalk refEntityCrossNew = new Crosswalk();
            boolean isRefEntityStatic = false;
            MultiValueCrosswalk refEntityCross = null;
            Crosswalk refEntityCrossWalk = null;

            if (refEntitiyObj != null && refEntitiyObj instanceof MultiValueCrosswalk) {
                refEntityCross = (MultiValueCrosswalk) attributeValues.get("refEntity");
                if (refEntityCross != null) {
                    refEntityCrossNew.type = crosswalk.type;

                    String inputVal = refEntityCross.value[multiValueIndex];
                    refEntityCrossNew.value = inputVal;
                    refEntity.getCrosswalks().add(refEntityCrossNew);
                    referenceValue.refEntity = refEntity;
                }

            } else if (refEntitiyObj instanceof Crosswalk) {
                isRefEntityStatic = true;

                refEntityCrossWalk = (Crosswalk) attributeValues.get("refEntity");
                if (refEntityCrossWalk != null) {

                    if (refEntityCrossWalk.type != null) {
                        refEntityCrossNew.type = refEntityCrossWalk.type;
                    } else {
                        refEntityCrossNew.type = crosswalk.type;
                    }


                    if (refEntityCrossWalk.sourceTable != null) {
                        refEntityCrossNew.sourceTable = refEntityCrossWalk.sourceTable;
                    }

                    String inputVal = refEntityCrossWalk.value;
                    refEntityCrossNew.value = inputVal;
                    refEntity.getCrosswalks().add(refEntityCrossNew);
                    referenceValue.refEntity = refEntity;
                }

            }

            Object refRelationObj = attributeValues.get("refRelation");

            Crosswalk refRelationCrosswalk = new Crosswalk();

            if (refRelationObj != null) {

                if (refEntitiyObj instanceof Crosswalk) {
                    refRelationCrosswalk = (Crosswalk) refRelationObj;
                    if (refRelationCrosswalk != null) {


                        if (refRelationCrosswalk.type != null) {
                            refRelationCrosswalk.type = refRelationCrosswalk.type;
                        } else {
                            refRelationCrosswalk.type = crosswalk.type;
                        }

                        if (refRelationCrosswalk.sourceTable != null) {
                            refRelationCrosswalk.sourceTable = refRelationCrosswalk.sourceTable;
                        }

                        refRelationCrosswalk.value = refRelationCrosswalk.value;

                    }

                }
            } else {
                if (crosswalk.value != null) {
                    refRelationCrosswalk.value = "R" + crosswalk.value.trim() + nestedAttributeName;
                } else {
                    refRelationCrosswalk.value = "R"
                            + RandomStringUtils.randomAlphanumeric(JsonGeneratorCoreProps.SIZE_OF_RANDOM_STRING - 3)
                            + nestedAttributeName;
                }
                if (isRefEntityStatic) {
                    refRelationCrosswalk.value += RandomStringUtils
                            .randomAlphanumeric(JsonGeneratorCoreProps.SIZE_OF_RANDOM_STRING);
                } else {
                    refRelationCrosswalk.value += refEntityCrossNew.value;
                }
                refRelationCrosswalk.type = crosswalk.type;
            }

            refRelation.getCrosswalks().add(refRelationCrosswalk);
            referenceValue.refRelation = refRelation;

            refAttrs.add(referenceValue);
            attributes.put(nestedAttributeName, refAttrs);
        }

        return isNonStaticExist;

    }

    /**
     * This is helper method to generate Crosswalk based on below inputs
     *
     * @param sourceSystem
     * @param crosswalkValue
     * @param isDataProvider
     * @param createDate
     * @param updateDate
     * @param deleteDate
     * @return Crosswalk Object
     */
    public static Crosswalk createCrosswalk(String sourceSystem, String crosswalkValue, Boolean isDataProvider,
                                            String createDate, String updateDate, String deleteDate) {
        Crosswalk crosswalk = new Crosswalk();
        crosswalk.type = sourceSystem;
        crosswalk.value = crosswalkValue;
        crosswalk.createDate = createDate;
        crosswalk.updateDate = updateDate;
        crosswalk.deleteDate = deleteDate;
        crosswalk.dataProvider = isDataProvider;

        return crosswalk;
    }

    /**
     * This is helper method to generate Crosswalk based on below inputs
     *
     * @param sourceSystem
     * @param crosswalkValue
     * @return Crosswalk Object
     */
    public static Crosswalk createCrosswalk(String sourceSystem, String crosswalkValue) {
        Crosswalk crosswalk = new Crosswalk();
        crosswalk.type = sourceSystem;
        crosswalk.value = crosswalkValue;
        return crosswalk;
    }

    /**
     * Checks whether the input value is not null
     *
     * @param value
     * @return true if not null, else false
     */
    public static boolean checkNotNull(String value) {
        return value != null
                //&& !value.trim().equals("")
                && !value.trim().equals("UNKNOWN")
                && !value.trim().equals("<blank>") && !value.trim().equals("<UNAVAIL>") && !value.trim().equals("#")
                //&& !value.toLowerCase().trim().equals("null")
                && !value.toLowerCase().trim().equals("\"");
    }

    /**
     * This is helper method for finding the attribute is Nested or Reference.
     * And based on that it will call the corresponding helper methods to do the
     * processing
     *
     * @param innerNestValues
     * @param attributes
     * @param attributeName
     * @param lineValues
     * @param columnIndexMap
     * @param nonColumnValues
     * @param crosswalk
     * @param columnDelimiter
     */
    private static void handleNestedReferenceValues(Map<String, Object> innerNestValues,
                                                    Map<String, Collection<Object>> attributes, String attributeName, String[] lineValues,
                                                    Map<String, Integer> columnIndexMap, Set<String> nonColumnValues, Crosswalk crosswalk,
                                                    String columnDelimiter, boolean isNullIgnore, String nestedDelimeter) {

        constructMultivalueMap(innerNestValues, attributeName);
        Boolean isNormalized = isNormalizedNestedRefAttribute(innerNestValues, lineValues, columnDelimiter);
        Integer sizeOfMultiValue = null;

        // Check the size of multi values
        if (isNormalized) {
            sizeOfMultiValue = getNormalizedNestedAttributeValueSize(innerNestValues, lineValues, columnDelimiter);
            if (sizeOfMultiValue == null) {
                return;
            }
        }

        if (innerNestValues.containsKey("refEntity") || innerNestValues.containsKey("refRelation")) {

            if (isNormalized) {

                Map<String, Object> attributeValuesNewMap = getNormalizedAttributeValueMap(innerNestValues, lineValues,
                        columnDelimiter, sizeOfMultiValue);


                if (multivalueMap.get(attributeName) != null && multivalueMap.get(attributeName)) {
                    for (int i = 0; i < sizeOfMultiValue; i++) {
                        createNormalizedReferenceAttributes(attributes, attributeName, attributeValuesNewMap, lineValues,
                                columnIndexMap, nonColumnValues, crosswalk, i, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                    }
                } else {
                    createNormalizedReferenceAttributes(attributes, attributeName, attributeValuesNewMap, lineValues,
                            columnIndexMap, nonColumnValues, crosswalk, 0, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                }

            } else {
                createReferenceAttributes(attributes, attributeName, innerNestValues, lineValues, columnIndexMap,
                        nonColumnValues, crosswalk, isNullIgnore);
            }

        } else {
            if (isNormalized) {

                Map<String, Object> attributeValuesNewMap = getNormalizedAttributeValueMap(innerNestValues, lineValues,
                        columnDelimiter, sizeOfMultiValue);

                if (multivalueMap.get(attributeName) != null && multivalueMap.get(attributeName)) {
                    for (int i = 0; i < sizeOfMultiValue; i++) {
                        createNormalizedNestedAttributes(attributes, attributeName, attributeValuesNewMap, lineValues,
                                columnIndexMap, nonColumnValues, crosswalk, i, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                    }
                } else {
                    createNormalizedNestedAttributes(attributes, attributeName, attributeValuesNewMap, lineValues,
                            columnIndexMap, nonColumnValues, crosswalk, 0, isNullIgnore, sizeOfMultiValue, nestedDelimeter);
                }

            } else {
                createNestedAttributes(attributes, attributeName, innerNestValues, lineValues, columnIndexMap,
                        nonColumnValues, crosswalk, isNullIgnore);
            }
        }

    }

    private static void constructMultivalueMap(Map<String, Object> attributeValues, String parentAttrName) {
        for (Map.Entry<String, Object> entry : attributeValues.entrySet()) {

            if (entry.getValue() instanceof String) {
                if (isMultiValue((String) entry.getValue())) {
                    multivalueMap.put(parentAttrName, true);
                }

            } else if (entry.getValue() instanceof Map) {
                constructMultivalueMap((Map<String, Object>) entry.getValue(), entry.getKey());
            }


        }
    }
}
