# Quick Start 
This guide is intended to get you up and running as quickly as possible. For a more detailed view of the utility please visit the [help portal](https://help.reltio.com/index.html#kb/kb_1178.html). 

##Building

The main method of the application varies based on the type of object to be generated. Below is the build path for each type.

1. Entity: **json-generator/src/main/java/com/reltio/etl/service/EntityJsonGeneratorService.java**
2. Interaction: **json-generator/src/main/java/com/reltio/etl/service/InteractionJsonGeneratorService.java**
3. Relation: **json-generator/src/main/java/com/reltio/etl/service/RelationJsonGeneratorService.java**

##Dependencies 

1. gson
2. reltio-cst-core-1.5.1


##Parameters File Example
The utility is executed with the the path of the parameters file as an argument.



##Entity
```
#Common Properties
THREAD_COUNT=Thread Count|20


#Util Specific Properties

#NOTE:Options "contributorProvider":true can be used only for non- "dataProvider" crosswalks.

INPUT_DATA_FILE=Path of the input source file|/mnt/project/src/sample.csv
INPUT_FILE_FORMAT=FLAT/CSV <<Input file format>> It should be either FLAT or CSV|CSV
INPUT_FILE_DELIMITER=|| (Only For Flat)|||
OUTPUT_FILE=Path of the output file that would be generated|/mnt/project/tgt/json-samples.json
MAPPING_FILE=Path of the mapping file that is defined|/mnt/project/prop/mapping.properties
ENTITY_TYPE=Reltio Entity type for which JSON is generated|configuration/entityTypes/Individual
SOURCE_SYSTEM=Source System|configuration/sources/Salesforce
SOURCE_TABLE=Source Table|KIE-US
CROSSWALK_VALUE_COLUMN=Value of the Crosswalk would be fetched from this column|ID
NON_DATAPROVIDER.SOURCE_SYSTEM=Non data provider source system|configuration/sources/IREP
NON_DATAPROVIDER.CROSSWALK_VALUE_COLUMN=NonDataProvider_Crosswalk
NON_DATAPROVIDER.CONTRIBUTOR_PROVIDER=TRUE
NORMALIZED_FILE_COLUMN_DELIMITER=Delimiter used for Normalized File Column. This column added to master after grouping the data|$#
NESTED_NORMALIZED_FILE_COLUMN_DELIMITER=This Delimiter is used inside the values of Normalized value. This further divides the normilized data. 
#Please refer to the testcases for more details of the format of the output.
JSON_OUTPUT_FORMAT=SINGLE_JSON / JSON_WITH_CROSSWALK_VALUE_COLUMN / JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN|SINGLE_JSON / JSON_WITH_CROSSWALK_VALUE_COLUMN / JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN
CROSSWALK_CREATE_DATE_COLUMN=Crosswalk Create Date Column|Create_date
CROSSWALK_UPDATE_DATE_COLUMN=Crosswalk Update Date Column|Update_date
CROSSWALK_DELETE_DATE_COLUMN=Crosswalk Delete Date Column|Delete_date
GENERATE_NULL_VALUES=To generate null values if needed|TRUE/FALSE
REFERENCE_ATTRIBUTE_SOURCE_SYSTEM=Reference Attribute Source System|configuration/sources/Salesforce
REFERENCE_RELATION_SOURCE_SYSTEM=Reference Attribute Source System|configuration/sources/Salesforce
REFERENCE_ATTRIBUTE_SOURCE_TABLE=Ref Entity Source Table
REFERENCE_RELATION_SOURCE_TABLE=Ref Relation Source Table


```

##Mapping File Example

```
#!paintext
sfdc_team_mapping.properties
FirstName=First_Name
LastName#1=Last Name
LastName#2=Last Name1
Phone.Number=[Mobilephone]
Phone.Type={Mobile}
Email#1.Type={Work}
Email#1.Email=Role
Email#2.Type={Work}
Email#2.Email#1=Username
Email#2.Email#2=Last Name
Address.AddressLine1=Address1
Address.City=City
Address.refEntity={surrogate}
Employment#1.Name=empname
Employment#1.refEntity=empId
Employment#2.Name=empname2
Employment#2.refEntity=empId2

```
##Relations

```


#Common Properties
THREAD_COUNT=Thread Count::10

#Util Specific Properties

INPUT_DATA_FILE=Path of the input source file::/mnt/project/src/sample.csv
INPUT_FILE_FORMAT=FLAT/CSV <<Input file format>> It should be either FLAT or CSV::CSV
INPUT_FILE_DELIMITER=|| (Only For Flat)::||
OUTPUT_FILE=Path of the output file that would be generated::/mnt/project/tgt/json-samples.json
MAPPING_FILE=Path of the mapping file that is defined::/mnt/project/prop/mapping.properties
RELATION_TYPE=Reltio Relation type for which JSON is generated::configuration/relationType/Manages
SOURCE_SYSTEM=Source System::configuration/sources/Salesforce
START_OBJECT_SOURCE_SYSTEM=Not mandatory if matches with SOURCE_SYSTEM::configuration/sources/Salesforce
START_OBJECT_CROSSWALK_VALUE_COLUMN=Relationship Start Object Crosswalk::configuration/sources/Salesforce
START_OBJECT_SOURCE_TABLE=Relationship Start Object Crosswalk Table Value::Email
START_OBJECT_ENTITY_URI_COLUMN=If Reltio Start Object in the Source Column is Entity URI::Uri_Start
END_OBJECT_SOURCE_SYSTEM=Not mandatory if matches with SOURCE_SYSTEM::configuration/sources/Salesforce
END_OBJECT_CROSSWALK_VALUE_COLUMN=Relationship End Object Crosswalk::Column_Name
END_OBJECT_SOURCE_TABLE=Relationship End Object Crosswalk Table Value::Source_Table
END_OBJECT_ENTITY_URI_COLUMN=If Reltio End Object in the Source Column is Entity URI::Uri_End
GENERATE_NULL_VALUES=If True will generate null values for empty records without ignoring::True/False
NORMALIZED_FILE_COLUMN_DELIMITER=Delimiter used for Normalized File Column. This column added to master after grouping the data::#$
JSON_OUTPUT_FORMAT=SINGLE_JSON / JSON_WITH_CROSSWALK_VALUE_COLUMN / JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN::SINGLE_JSON / JSON_WITH_CROSSWALK_VALUE_COLUMN / JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN
CROSSWALK_CREATE_DATE_COLUMN=Crosswalk Create Date Column::Create_date
CROSSWALK_UPDATE_DATE_COLUMN=Crosswalk Update Date Column::Update_date
CROSSWALK_DELETE_DATE_COLUMN=Crosswalk Delete Date Column::Delete_date
RELATION_CROSSWALK_VALUE_COLUMN=Value of the Crosswalk would be fetched from this column::Column_Name
SOURCE_TABLE=Source Table::Source_Table
RELATION_START_DATE_COLUMN=Start Date for Relation::Column_Name
RELATION_END_DATE_COLUMN=End Date for Relation::Column_Name
IGNORE_RELATION_CROSSWALK=To Ignore Crosswalk creation in Relation JSON generation::Column_Name

```

##Mapping File Sample
```
Type=Type
```

##Interactions
```

#Common Properties
THREAD_COUNT=Thread Count::20

#Util Specific Properties
INPUT_DATA_FILE=Path of the input source file::/mnt/project/tgt/input.txt
INPUT_FILE_FORMAT=Input file format::CSV/FLAT
INPUT_FILE_DELIMITER=|| (Only For Flat)::||
OUTPUT_FILE=Path of the output file that would be generated::/mnt/project/tgt/json-samples.json
ATTRIBUTE_MAPPING_FILE=Path of the attribute mapping file that is defined::/mnt/project/tgt/attribute-mapping.properties
MEMBERS_MAPPING_FILE=Path of the members mapping file that is defined::/mnt/project/tgt/member-mapping.properties
INTERACTION_TYPE=Interaction Type::configuration/interactionTypes/ProdLog
SOURCE_SYSTEM=Source System::configuration/sources/Salesforce
CROSSWALK_VALUE_COLUMN=Value of the Crosswalk would be fetched from this column::ID
NORMALIZED_FILE_COLUMN_DELIMITER=Delimiter used for Normalized File Column. This column added to master after grouping the data::$#
GENERATE_NULL_VALUES=To generate null values if needed::TRUE/FALSE
JSON_OUTPUT_FORMAT=SINGLE_JSON / JSON_WITH_CROSSWALK_VALUE_COLUMN / JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN::SINGLE_JSON / JSON_WITH_CROSSWALK_VALUE_COLUMN / JSON_WITH_OUT_CROSSWALK_VALUE_COLUMN
TIMESTAMP_COLUMN=Set TimeStamp for Interaction::
```
##Attribute Mapping

```
Example: attribute_mapping.properties
PROD_INJ_DATE=Date
DAYS_PROD=DaysProd
DAYS_INJECT=DaysInject
OIL_PROD=OilProd
COND_PROD=CondProd
OWG_PROD=OWGProd
GWG_PROD=GWGProd
SUPPLY_WATER_PROD=SupplyWaterProd
WATER_PROD=WaterProd
WATER_INJ=WaterInj
GAS_INJ=GasInj
DISP_WATER_INJ=DispWaterInj
CYCLIC_STEAM_INJ=CyclicSteamInj
STEAM_INJ=SteamInj

```

##Members Mapping

```
member_mapping.properties

WellComponent.SOURCE_SYSTEM=configuration/sources/ODS
WellComponent.CROSSWALK_VALUE_COLUMN=API_NO14
```





##Executing

Command to start the utility.
```
#!plaintext

java -jar json-generator-{{data-type}}-{{version}}.jar config.properties > $logfilepath$

```
